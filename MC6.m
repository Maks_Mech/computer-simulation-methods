clc;
clear;

rng('shuffle', 'simdTwister') % ustawienie generatora 

abc = 'Podaj ilość rzutów n - seria to 2n rzutów (zal. do 1000): ';
n = input(abc);
abc = 'Podaj ilość przeprowadzonych gier (zal. do 1000): ';
prob = input(abc);
%n = 1000;
%prob = 1000000;

for i = 1:prob; % tworzenie macierzy na wyniki
    dane(i) = gra(n);
end;

nbins = n+1; %ilość słupków na wykresie 
histogram(dane,nbins)
title('Rozkład zmiennej losowej R')
ylabel('Ilość gier, w których wystąpił remis')
xlabel('Wartość zmiennej losowej R')

format shortg % sprawdzanie aktualnego czasu
zegar = clock;

nazwa = sprintf('wyniki/MC6_(matlab)_%d_%d_%d_%d_%d_%d.png', zegar(1), zegar(2), zegar(3), zegar(4), zegar(5), round(zegar(6)));
saveas(gcf,nazwa); % zapisywanie pliku png

nazwa2 = sprintf('wyniki/MC6_(matlab)_%d_%d_%d_%d_%d_%d.txt', zegar(1), zegar(2), zegar(3), zegar(4), zegar(5), round(zegar(6)));
plik = fopen(nazwa2,'w+'); % zapisywanie danych do pliku txt
for i = 1:prob
    fprintf(plik,'%d\n',dane(i));
end
fclose(plik);

function R = gra(n)
    suma_a = 0; % zerowanie stanów początkowych
    max_0 = 0;
    for i = 1:2*n; 
       rzut = randi([0,1]);
       if rzut == 0; %jeśli reszka, wtedy traci stąd podmiana 0 na -1
           rzut = -1;
       end;
       suma_a = suma_a + rzut; 
       if suma_a == 0; %znajdowanie maksimum
           max_0 = i;
       end;
    end;
    R = max_0/2; %maksimum może wystąpić tylko w parzystym rzucie
end